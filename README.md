### Tray Gerbes Matos
Este projeto tem o objetivo de consultar 10 anúncios com as suas visitas no mercadolivre usando uma palavra-chave.

Infelizmente não consegui obter os dados do endpoint /visits sem a autenticação da API.

![img](./doc/t99.png)

Desta forma, criei um servidor proxy com nginx para o meu ambiente de desenvolvimento. No link abaixo, é possível efetuar o cadastro e login para validação do projeto.

[https://tray-gmatos.ddns.net/](https://tray-gmatos.ddns.net/)

#### Features
- [X] Autenticação MercadoLivre
- [X] Cadastro e exclusão de palavras
- [X] Exclusão de anúncios
- [ ] Autenticação JWT
- [ ] Paginação da API

### Dashboard 
Durante o primeiro acesso, é necessário clicar no link "Mercadolibre Oauth" para que o sistema continue a sua atualização contínua.

Devido à natureza do projeto, não implementei a autenticação com o refresh token, sendo necessário atualizar o token a cada 3 horas.


![img](./doc/t0.png)

### Palavras
Cadastro e exclusão de palavras

![img](./doc/t2.png)

### Anúncios

Acompanhamento e exclusão de anúncios

![img](./doc/t1.png)

## API

``
https://tray-gmatos.ddns.net/api/items
``

![img](./doc/t5.png)

Links
- [ALL ITEMS](https://tray-gmatos.ddns.net/api/items)
- [ALL ITEMS WITH WORD_ID = 1](https://tray-gmatos.ddns.net/api/items?word_id=1) (iphone 14)

## Ambiente

No projeto foi usado o Laravel Sail para criação dos containers.

[Laravel Sails](https://laravel.com/docs/10.x/sail)
