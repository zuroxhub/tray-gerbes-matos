<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('items', function (Blueprint $table) {

            $table->id();

            $table->unsignedBigInteger('word_id');

            $table->foreign('word_id')->references('id')->on('words')->onDelete('cascade');

            $table->string('item_id');

            $table->string('title',225)->nullable();

            $table->bigInteger('visits')->default('0')->nullable();

            $table->enum('status',['Em processamento','Processado','Falha na consulta'])->default('Em processamento');

            $table->timestamp('updated');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('items');
    }
};
