<?php

namespace Database\Seeders;

use App\Models\Word;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class WordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $words = [
            'iphone 14'
        ];

        foreach ($words as $word){

            Word::firstOrCreate([
                'name' => $word,
                'is_active' =>true
            ]);

        }

    }
}
