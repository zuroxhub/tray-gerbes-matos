<?php

namespace App\Http\Middleware;

use App\Actions\DecodeTokenJwt;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class ApiAutenticationJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {


        #$jwt = $request->bearerToken();
        $jwt = $request->get('token');
       # $decoded = DecodeTokenJwt::run( $jwt );
        $key = config('tray.jwt_secret');



        $decoded = JWT::decode($jwt, new Key($key, 'HS256'));
        dd($decoded);

        return $next($request);
    }
}
