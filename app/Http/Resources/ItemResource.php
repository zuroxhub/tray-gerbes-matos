<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'word_id' => $this->word_id,
            'item_id' => $this->item_id,
            'title' => $this->title,
            'visits' => $this->visits,
            'status' => $this->status,
            'updated' => \Carbon\Carbon::parse($this->updated)->format('Y-m-d H:i:s'),  // Formatando a data
        ];
    }
}
