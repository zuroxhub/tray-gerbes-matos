<?php

namespace App\Http\Controllers;

use App\Actions\ClearAcessToken;
use App\Actions\MeliAuthorizationCode;
use App\Actions\MeliCreateAuthorizationLink;
use App\Actions\SaveAcessToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Ixudra\Curl\Facades\Curl;

class MercadoLibreAuthController extends Controller
{

    /**
     * Redirect To Auth
     *
     */
    public function redirectToAuth()
    {
        $auth_uri = MeliCreateAuthorizationLink::run();
        return redirect($auth_uri);
    }

    /**
     * Redirect Code
     *
     */
    public function redirectedCode()
    {
        $code = request()->get('code');

        // Obtem o access token
        $authorization = MeliAuthorizationCode::run( $code );

        if( isset($authorization->access_token)){
            SaveAcessToken::run( $authorization->access_token );
            return redirect('/dashboard');
        }

        throw new \Exception('Erro ao obter o autorização');

    }

    /**
     * Logout Session
     *
     */
    public function logoutSession (){

        // Clear AccessToken
        ClearAcessToken::run();

        return redirect('/dashboard');
    }

}
