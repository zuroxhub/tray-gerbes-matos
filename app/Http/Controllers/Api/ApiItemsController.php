<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemsResource;
use App\Models\Item;
use App\Services\ResponseService;
use Illuminate\Http\Request;

class ApiItemsController extends Controller
{

    public $res;

    public function __construct( ResponseService $responseService ){
        $this->res = $responseService;
    }

    public function collection( Request $request)
    {
        $query = Item::query();

        if($request->has('word_id') && $request->get('word_id'))
            $query = $query->where('word_id',$request->get('word_id'));

        $query = $query->get();
        $data = new ItemsResource( $query );
        return $this->res->success($data);
    }
}
