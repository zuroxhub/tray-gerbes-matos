<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\MeliCredentials;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Inertia\Inertia;
use App\Jobs\WordSearchJob;

class AdminController extends Controller
{
    public function adminDashboard()
    {

        $accessToken = null;

        // Busca no banco de dados uma sessão existente
        $findEntityCredentials = MeliCredentials::first();
        if( isset($findEntityCredentials->access_token) && $findEntityCredentials->access_token)
            $accessToken = $findEntityCredentials->access_token;

        return Inertia::render('Dashboard', compact('accessToken'));
    }

    public function wordsDashboard()
    {
        $words = Word::get();
        return Inertia::render('Words', compact('words'));
    }

    public function wordToogle( Request $request )
    {

        Word::where('id', $request->get('id'))->update([
            'is_active' => $request->get('is_active')
        ]);

        $entity = Word::where('id', $request->get('id'))->first();

        dispatch( new WordSearchJob( $entity ) );

        return response()->json([
            'success' => true,
        ]);

    }

    public function wordCreate( Request  $request)
    {
        $createdWord = Word::firstOrCreate( [
            'name' => $request->get('name'),
            'is_active' => true
        ]);

        dispatch( new WordSearchJob( $createdWord ) );

        return redirect(route('admin.words'));
    }

    public function wordDestroy( $id )
    {
        Word::where('id',$id)->delete();

        return redirect(route('admin.words'));
    }

    public function itemsDashboard()
    {

        $items = Item::query();

        $items = $items->with('word');

        $items = $items->get();

        return Inertia::render('Items',compact('items'));
    }

    public function itemDestroy( $id )
    {
        $entity = Item::where('id',$id)->delete();
        return redirect()->back();
    }

}
