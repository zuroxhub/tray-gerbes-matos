<?php

namespace App\Services;

use App\Models\Word;
use Ixudra\Curl\Facades\Curl;

class WordsService
{

    public function getActiveWords()
    {
        return Word::active()->get();
    }

}
