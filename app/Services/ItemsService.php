<?php

namespace App\Services;

use App\Models\Item;
use App\Models\Word;
use Ixudra\Curl\Facades\Curl;

class ItemsService
{

    /**
     * Count Active Items By Word
     *
     * @param Word $word
     * @return mixed
     */
    public function countActiveItemsByWord( Word $word )
    {
        return Item::where('word_id',$word->id)->count();
    }

    /**
     * Get Active Items By Word
     *
     * @param Word $word
     * @return mixed
     */
    public function getActiveItemsByWord( Word $word )
    {
        return Item::where('word_id', $word->id)->get();
    }

}
