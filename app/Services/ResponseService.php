<?php

namespace App\Services;

class ResponseService {

    public function success($data = [], $message = 'Operação bem-sucedida', $code = 200) {
        return $this->formatResponse('success', $message, $data, $code);
    }

    public function error($message = 'Ocorreu um erro', $code = 400, $data = []) {
        return $this->formatResponse('error', $message, $data, $code);
    }

    public function notFound($message = 'Recurso não encontrado', $data = []) {
        return $this->error($message, 404, $data);
    }

    public function unauthorized($message = 'Não autorizado', $data = []) {
        return $this->error($message, 401, $data);
    }

    public function validationError($message = 'Erro de validação', $errors = []) {
        return $this->formatResponse('error', $message, ['errors' => $errors], 422);
    }

    private function formatResponse($status, $message, $data = [], $code = 200) {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }
}
