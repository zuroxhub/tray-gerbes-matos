<?php

namespace App\Services;

use App\General;
use Ixudra\Curl\Facades\Curl;

class MercadoLibreService
{

    // Endpoint da API MercadoLivre
    private $baseUrl = 'https://api.mercadolibre.com';

    // Endpoint para autenticação
    private $oauthUrl = 'https://auth.mercadolivre.com.br';


    /**
     * Get Search Items
     * Retorna uma busca de anúncios no Marketplace
     *
     * @param $termSearch
     * @return array|mixed|\stdClass
     */
    public function getSearchItems( string $termSearch, ?int $page = null, $limit = 50 ): array
    {
        $page = $page ?: 1;

        $params = [
            'q' => $termSearch,
            'page' => $page,
            'limit' => $limit,
        ];

        $dataRequested = Curl::to( $this->baseUrl . '/sites/MLB/search?' . http_build_query($params) )
            ->asJson()
            ->get();

        // Retorna os dados no formato Array
        return General::objectToArray( $dataRequested );

    }


    /**
     * Get Item By ID
     * Retorna os detalhes de um anúncio MercadoLivre
     *
     * @param $itemId
     * @return array|mixed|\stdClass
     */
    public function getItemById( string $itemId)
    {
        return Curl::to($this->baseUrl . '/items/' . $itemId)
            ->get();
    }

    /**
     * Multi Get Items By IDs
     *
     *
     * @param $itemsIds
     * @return array|mixed|\stdClass
     */
    public function multiGetItemsByIDs( array $itemsIds )
    {
        $dataRequested = Curl::to($this->baseUrl . '/items?ids=' . implode(',',$itemsIds))
            ->asJson()
            ->get();

        // Retorna os dados no formato Array
        return General::objectToArray( $dataRequested );

    }

    public function getItemVisits( string $itemId, $access_token = false )
    {

        $dataRequested = Curl::to($this->baseUrl . '/visits/items?ids=' . $itemId );

        if($access_token!=false)
            $dataRequested = $dataRequested->withBearer($access_token);

        $dataRequested = $dataRequested->asJson()
            ->get();

        $arrayData = General::objectToArray($dataRequested);

        if( isset($arrayData[ $itemId]))
            return $arrayData[$itemId];

        return null;
    }

}
