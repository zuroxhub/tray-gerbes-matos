<?php

namespace App\Jobs;

use App\DTOs\ItemData;
use App\Models\Item;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ItemUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $itemData;

    /**
     * Create a new job instance.
     */
    public function __construct( ItemData $itemData )
    {
        $this->itemData = $itemData;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        // Converte os dados em array
        $payload = $this->itemData->toArray();

        $entity = Item::firstOrCreate([
            'word_id' => $payload['word_id'],
            'item_id' => $payload['item_id'],
        ]);

        // Salvando o titulo na primeira interação
        if(!$entity->title)
            $entity->title = $payload['title'];

        $entity->status = 'Em processamento';
        $entity->save();

        // Atualiza as visitas do anúncio
        dispatch( new ItemVisitsJob( $payload['item_id'] ));

    }
}
