<?php

namespace App\Jobs;

use App\DTOs\ItemData;
use App\Models\Word;
use App\Services\ItemsService;
use App\Services\MercadoLibreService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WordSearchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $word;

    protected $pendingWords;

    public function __construct( Word $word, $pendingItems = 0 )
    {
        $this->word = $word;
        $this->pendingWords = $pendingItems;
    }

    public function handle( MercadoLibreService $mercadoLibreService): void
    {

        // Palavra
        $word_name = $this->word['name'];

        // Limite de anúncios por palavra
        $limit = config('tray.limit_items_per_word');

        // Busca os anúncios usando a palavra e o limite
        $itemsSearchResults = $mercadoLibreService->getSearchItems( $word_name, 1, $limit );

        if(!isset($itemsSearchResults['results']))
            throw new \Exception('Não foi possivel obter anúncios durante a pesquisa.',500);

        if( count($itemsSearchResults['results']) < $limit )
            throw new \Exception('Não há anúncios suficiente para executar esta operação',500);

        foreach ($itemsSearchResults['results'] as $item){

            $itemData = new ItemData( [
                'word_id' => $this->word['id'],
                'item_id' => $item['id'],
                'title' => $item['title']
            ]);

            // Disparaa job para sincronização do anúncio
            dispatch( new ItemUpdateJob( $itemData ));

        }

    }
}
