<?php

namespace App\Jobs;

use App\Actions\GetAccessToken;
use App\Models\Item;
use App\Services\MercadoLibreService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ItemVisitsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $item_id;

    /**
     * Create a new job instance.
     */
    public function __construct( string $item_id )
    {
        $this->item_id = $item_id;
    }

    /**
     * Execute the job.
     */
    public function handle( MercadoLibreService $mercadoLibreService ): void
    {

        try {
            $this->saveVisits( $mercadoLibreService );
        }catch ( \Exception $exception) {
            $itemEntity = Item::where('item_id', $this->item_id)->first();
            $itemEntity->status = 'Falha na consulta';
            $itemEntity->updated = Carbon::now()->format('Y-m-d H:i:s');
            $itemEntity->save();
        }


    }

    protected function saveVisits( $mercadoLibreService )
    {
        // Obtem o access_token do banco de dados
        $accessToken = GetAccessToken::run( true );
        if(!$accessToken)
            throw new \Exception('access token do mercadolivre não está disponível');

        // Consulta o total de visitas usando o access_tken
        $totalVisits = $mercadoLibreService->getItemVisits( $this->item_id, $accessToken );

        // Seleciona o item para atualização
        $itemEntity = Item::where('item_id', $this->item_id)->first();
        $itemEntity->visits = $totalVisits;
        $itemEntity->status = 'Processado';
        $itemEntity->updated = Carbon::now()->format('Y-m-d H:i:s');
        $itemEntity->save();
    }

}
