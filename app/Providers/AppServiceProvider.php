<?php

namespace App\Providers;

use App\Services\ItemsService;
use App\Services\MercadoLibreService;
use App\Services\WordsService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {

        if( config('tray.proxy_ssl') == true ) {
            $this->app['request']->server->set('HTTPS', true);
        }

        $this->app->bind(MercadoLibreService::class, function ($app) {
            return new MercadoLibreService();
        });
        $this->app->bind(ItemsService::class, function ($app) {
            return new ItemsService();
        });
        $this->app->bind(WordsService::class, function ($app) {
            return new WordsService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
