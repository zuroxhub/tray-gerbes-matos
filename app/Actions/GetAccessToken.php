<?php

namespace App\Actions;

use App\Models\MeliCredentials;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * Class GetAccessToken
 * Retorna o access token mercadolivre
 * Tenta buscar no banco de dados uma sessão válida
 *
 * @package App\Actions
 */
class GetAccessToken
{
    use AsAction;

    public function handle( $command_line = true  )
    {

        // Durante a execução de comandos/jobs
        if( $command_line && Cache::has('access_token_cached')){
            return Cache::get('access_token_cached');
        }

        $uniqueEntity = MeliCredentials::first();

        if( isset($uniqueEntity->access_token) ){
            Cache::put('access_token_cached',$uniqueEntity->access_token,10800);
            return $uniqueEntity->access_token;
        }

        return false;

    }


}
