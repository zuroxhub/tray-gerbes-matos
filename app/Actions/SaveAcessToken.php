<?php

namespace App\Actions;

use App\Models\MeliCredentials;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Lorisleiva\Actions\Concerns\AsAction;

class SaveAcessToken
{
    use AsAction;

    public function handle( $access_token )
    {

        // Default Meli Expires
        $expires_at = Carbon::now()->addHours(3);

        // Save token on session
        Session::put('meli_access_token', $access_token);

        // Save token on database
        $uniqueEntity = MeliCredentials::first();
        if(!isset($uniqueEntity->id)){
            $uniqueEntity = MeliCredentials::create([
                'access_token' => $access_token,
                'expires_at' => $expires_at
            ]);
        }else{

            $uniqueEntity->access_token = $access_token;
            $uniqueEntity->expires_at = Carbon::now()->addHours(3);
            $uniqueEntity->save();

        }


    }
}
