<?php

namespace App\Actions;

use Ixudra\Curl\Facades\Curl;
use Lorisleiva\Actions\Concerns\AsAction;

class MeliAuthorizationCode
{
    use AsAction;

    public function handle( $code )
    {

        $base_url = 'https://api.mercadolibre.com/oauth/token';

        $body = array(
            "grant_type" => "authorization_code",
            "client_id" => config('tray.meli_id'),
            "client_secret" => config('tray.meli_secret'),
            "code" => $code,
            "redirect_uri" => config('tray.meli_redirect_url'),
        );

        return Curl::to($base_url)
            ->withData($body)
            ->asJson()
            ->post();

    }
}
