<?php

namespace App\Actions;

use Lorisleiva\Actions\Concerns\AsAction;

class MeliCreateAuthorizationLink
{
    use AsAction;

    public function handle()
    {

        $auth_url = 'https://auth.mercadolivre.com.br';
        $redirect_url = config('tray.meli_redirect_url');

        $params = array("client_id" => config('tray.meli_id'), "response_type" => "code", "redirect_uri" => $redirect_url);
        return $auth_url . "/authorization?" . http_build_query($params);

    }
}
