<?php

namespace App\Actions;

use App\Models\MeliCredentials;
use Illuminate\Support\Facades\Session;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * Class ClearAcessToken
 * Limpa o access token mercadolivre.
 * Tambem é removido do banco de dados
 *
 * @package App\Actions
 */
class ClearAcessToken
{
    use AsAction;

    public function handle()
    {

        // Clear Session
        Session::forget('meli_access_token');

        // Clear Database
        $EntityCredentials = MeliCredentials::first();
        if($EntityCredentials) {
            $EntityCredentials->access_token = null;
            $EntityCredentials->save();
        }

    }
}
