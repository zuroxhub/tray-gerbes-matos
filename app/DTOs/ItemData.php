<?php

namespace App\DTOs;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ItemData
{
    public string $word_id;
    public string $item_id;
    public string $title;

    public function __construct( array $data )
    {

        $validator = Validator::make($data, [
            'word_id' => 'required|numeric',
            'item_id' => 'required|string',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->word_id = $data['word_id'];
        $this->item_id = $data['item_id'];
        $this->title = $data['title'];

    }

    public function toArray(): array
    {
        return [
            'word_id' => $this->word_id,
            'item_id' => $this->item_id,
            'title' => $this->title
        ];
    }

}
