<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Word extends Model
{
    use HasFactory;

    public $fillable = [
        'name',
        'is_active'
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    /**
     * Scope Query | Status Active
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive( Builder $query ): Builder
    {
        return $query->where('is_active',true);
    }

}
