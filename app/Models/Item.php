<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    public $fillable = [
        'item_id',
        'word_id',
        'title',
        'status',
        'visits',
        'updated'
    ];

    protected $casts = [
        'updated' => 'datetime'
    ];

    public $timestamps = false;


    public function word()
    {
        return $this->hasOne(Word::class,'id','word_id');
    }

    public function save(array $options = [])
    {
        if ($this->isDirty()) {
            $this->updated = Carbon::now();
        }
        return parent::save($options);
    }


}
