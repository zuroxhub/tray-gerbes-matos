<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MeliCredentials extends Model
{
    use HasFactory;

    public $fillable = [
        'access_token',
        'expires_at'
    ];

    protected $casts =[
        'expires_at' => 'datetime'
    ];

    public $timestamps = false;

}
