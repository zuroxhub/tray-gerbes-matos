<?php

namespace App\Console\Commands;

use App\Models\MeliCredentials;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearMercadoLivreToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:clear-mercado-livre-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $tokens = MeliCredentials::get();

        foreach ($tokens as $token){
            if( Carbon::parse($token->expires_at)->isPast() ){
                $token->delete();
            }
        }

    }
}
