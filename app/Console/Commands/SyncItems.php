<?php

namespace App\Console\Commands;

use App\DTOs\ItemData;
use App\Jobs\ItemUpdateJob;
use App\Jobs\ItemVisitsJob;
use App\Jobs\WordSearchJob;
use App\Models\Word;
use App\Services\ItemsService;
use App\Services\MercadoLibreService;
use App\Services\WordsService;
use Illuminate\Console\Command;

class SyncItems extends Command
{
    // The name and signature of the console command.
    protected $signature = 'app:sync-items';

    // The console command description.
    protected $description = 'Command description';

    // Limit Items Per Word
    protected $maxItemsPerWord = 0;

    /**
     * Execute the console command.
     */
    public function handle( WordsService $wordsService, ItemsService $itemsService, MercadoLibreService $mercadoLibreService )
    {

        // Carrega o limite de anúncios por palavra
        $this->maxItemsPerWord = config('tray.limit_items_per_word');

        // Obtem as palavras ativas
        $activeWords = $wordsService->getActiveWords();

        foreach ( $activeWords as $word ){

            // Conta quantos anúncios existem com a palavra
            $totalItemsByWord = $itemsService->countActiveItemsByWord( $word );

            // Valida se a quantidade máxima de itens por anúncio existe no banco de dados
            if( $this->maxItemsPerWord > $totalItemsByWord ){

                // Quantide pendente
                $quantityPendig = $this->maxItemsPerWord - $totalItemsByWord;

                dispatch( new WordSearchJob( $word, $quantityPendig));

            }

            if( $this->maxItemsPerWord == $totalItemsByWord ){
                $activeItems = $itemsService->getActiveItemsByWord( $word );
                $activeItemsIds = $activeItems->pluck('item_id')->toArray();
                $activeItemsData = $mercadoLibreService->multiGetItemsByIDs( $activeItemsIds );
                foreach ($activeItemsData as $item){

                    $itemRawData = $item['body'];
                    $itemData = new ItemData( [
                        'word_id' => $word['id'],
                        'item_id' => $itemRawData['id'],
                        'title' => $itemRawData['title']
                    ]);

                    // Disparaa job para sincronização do JOB
                    dispatch( new ItemUpdateJob( $itemData ));
                   # dispatch( new ItemVisitsJob( $itemRawData['id'] ));

                }
            }



        }

    }

}
