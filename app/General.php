<?php

namespace App;

class General {

    /**
     * Object To Array
     *
     * @param $object
     * @return array|mixed
     */
    public static function objectToArray($object) {
        if (is_object($object)) {
            $object = get_object_vars($object);
        }

        if (is_array($object)) {
            return array_map([self::class, 'objectToArray'], $object);
        }

        return $object;
    }

}
