<?php

return [

    // JWT Secret
    'jwt_secret' => env('JWT_KEY'),

    // JWT Expires time
    'jwt_expires' => env('JWT_EXP'),

    // Define se o sistema está rodando via proxy https, forçando o https no projeto
    'proxy_ssl' => env('APP_SSL_PROXY') ? env('APP_SSL_PROXY') : true,

    // Limite de anúncios por palavra
    'limit_items_per_word' => env('LIMIT_ITEMS_PER_WORD') ?: 10,

    // ID da aplicação no MercadoLivre
    'meli_id' => env('MELI_ID'),

    // Segredo da aplicação no MercadoLivre
    'meli_secret' => env('MELI_SECRET'),

    // Url de redirecionamento da autenticação MercadoLivre
    'meli_redirect_url' => env('MELI_REDIRECT_URL')

];
