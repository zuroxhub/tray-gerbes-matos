<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard',[\App\Http\Controllers\AdminController::class,'adminDashboard'])->middleware(['auth', 'verified'])->name('dashboard');
Route::get('/admin/words',[\App\Http\Controllers\AdminController::class,'wordsDashboard'])->middleware(['auth', 'verified'])->name('admin.words');
Route::post('/admin/words/create',[\App\Http\Controllers\AdminController::class,'wordCreate'])->middleware(['auth', 'verified'])->name('admin.words.create');
Route::post('/admin/words/toogle',[\App\Http\Controllers\AdminController::class,'wordToogle'])->middleware(['auth', 'verified'])->name('admin.words.toogle');
Route::delete('/admin/words/destroy/{id}',[\App\Http\Controllers\AdminController::class,'wordDestroy'])->middleware(['auth', 'verified'])->name('admin.words.destroy');
Route::get('/admin/items',[\App\Http\Controllers\AdminController::class,'itemsDashboard'])->middleware(['auth', 'verified'])->name('admin.items');
Route::delete('/admin/item/destroy/{id}',[\App\Http\Controllers\AdminController::class,'itemDestroy'])->middleware(['auth', 'verified'])->name('admin.items.destroy');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::prefix('mercadolibre')->group(function(){
       Route::get('oauth-redirect',[\App\Http\Controllers\MercadoLibreAuthController::class,'redirectToAuth'])->name('meli.oauth');
       Route::get('session-logout',[\App\Http\Controllers\MercadoLibreAuthController::class,'logoutSession'])->name('meli.oauth');
    });
});

Route::get('redirected',[\App\Http\Controllers\MercadoLibreAuthController::class,'redirectedCode'])->name('meli.redirected');

Route::get('redirect-to-marketplace/{mlb}',function ($mlb){
   $mlb = str_replace('MLB','MLB-',$mlb);
   return redirect('https://produto.mercadolivre.com.br/'.$mlb);
});

require __DIR__.'/auth.php';
