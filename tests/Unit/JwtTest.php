<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Firebase\JWT\Key;
use Firebase\JWT\JWT;

class JwtTest extends TestCase
{
    private $key = "eyJhbGciOiJIUzI1NiJ9.ew0KICAic3ViIjogIjEyMzQ1Njc4OTAiLA0KICAibmFtZSI6ICJBbmlzaCBOYXRoIiwNCiAgImlhdCI6IDE1MTYyMzkwMjINCn0.1ZSAtX668JJ6Olmku3Kt5XNclbzXNik-YcOSX6XfKFA";  // Substitua pela sua chave secreta

    public function testJwtTokenCreation()
    {
        $payload = [
            "iss" => "traycorp",
            "aud" => "validation",
            "iat" => time(),
            "exp" => time() + 3600,
            "data" => [
                "id" => 1,
                "name" => "Gerbes Matos"
            ]
        ];

        $jwt = JWT::encode($payload, $this->key, 'HS256');

        $this->assertNotEmpty($jwt, "Aconteceu um erro durante a criação do Token JWT");
    }

    public function testJwtTokenDecoding()
    {
        $payload = [
            "iss" => "traycorp",
            "aud" => "validation",
            "iat" => time(),
            "exp" => time() + 3600,
            "data" => [
                "id" => 1,
                "name" => "Gerbes Matos"
            ]
        ];

        $jwt = JWT::encode($payload, $this->key, 'HS256');

        $decodedPayload = JWT::decode($jwt,  new Key($this->key, 'HS256'));

        $this->assertEquals($payload['data']['id'], $decodedPayload->data->id, "Houve um erro na decodificação do JWT");
        $this->assertEquals($payload['data']['name'], $decodedPayload->data->name, "Houve um erro na decodificação do JWT");
    }
}
